package br.com.pan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.pan.model.EnderecoEntity;



@Repository
public interface EnderecoRepository extends JpaRepository<EnderecoEntity, Long>{

	
	EnderecoEntity findByCep(String cep);
}
