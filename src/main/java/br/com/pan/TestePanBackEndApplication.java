package br.com.pan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients 
public class TestePanBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestePanBackEndApplication.class, args);
	}

	
	
}
