package br.com.pan.model.builder;

import br.com.pan.model.ClienteEntity;
import br.com.pan.model.EnderecoEntity;

public class ClienteEntityBuilder implements Builder<ClienteEntity>{

private ClienteEntity clienteEntity;
	
	
	public ClienteEntityBuilder() {
		this.clienteEntity = new ClienteEntity();
	}

	public static ClienteEntityBuilder builder() {
		return new ClienteEntityBuilder();
	}
	
	public ClienteEntityBuilder withId(Long id) {
		this.clienteEntity.setId(id);
		return this;
	}

	public ClienteEntityBuilder withNome(String nome) {
		this.clienteEntity.setNome(nome);
		return this;
	}
	
	public ClienteEntityBuilder withEndereco(EnderecoEntity endereco) {
		this.clienteEntity.setEndereco(endereco);
		return this;
	}
		
	@Override
	public ClienteEntity build() {
		return this.clienteEntity;
	}

}
