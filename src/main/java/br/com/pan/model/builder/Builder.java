package br.com.pan.model.builder;

public interface Builder<T> {

	T build();
}
