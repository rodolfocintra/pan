package br.com.pan.model.builder;

import br.com.pan.model.EnderecoEntity;

public class EnderecoEntityBuilder implements Builder<EnderecoEntity>{

	private EnderecoEntity enderecoEntity;
	
	
	
	public EnderecoEntityBuilder() {
		this.enderecoEntity = new EnderecoEntity();
	}

	public static EnderecoEntityBuilder builder() {
		return new EnderecoEntityBuilder();
	}
	
	public EnderecoEntityBuilder withId(Long id) {
		this.enderecoEntity.setId(id);
		return this;
	}

	public EnderecoEntityBuilder withCep(String cep) {
		this.enderecoEntity.setCep(cep);
		return this;
	}
	
	public EnderecoEntityBuilder withLogradouro(String logradouro) {
		this.enderecoEntity.setLogradouro(logradouro);
		return this;
	}
	public EnderecoEntityBuilder withNumero(String numero) {
		this.enderecoEntity.setNumero(numero);
		return this;
	}
	
	public EnderecoEntityBuilder withComplemento(String complemento) {
		this.enderecoEntity.setComplemento(complemento);
		return this;
	}
	
	public EnderecoEntityBuilder withBairro(String bairro) {
		this.enderecoEntity.setBairro(bairro);
		return this;
	}
	
	public EnderecoEntityBuilder withUf(String uf) {
		this.enderecoEntity.setUf(uf);
		return this;
	}
	

	@Override
	public EnderecoEntity build() {
		return this.enderecoEntity;
	}

}
