package br.com.pan.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.pan.cliente.api.model.Estado;
import br.com.pan.cliente.api.model.Municipio;


@FeignClient(value = "${client.feing.ibge.name}",url="${client.feing.ibge.url}")
public interface IbgeClient {

	@RequestMapping(method = RequestMethod.GET, value = "localidades/estados/")
	List<Estado> getEstados();
	
	@RequestMapping(method = RequestMethod.GET, value = "localidades/estados/{id}/municipios")
	List<Municipio> getMunicipio(@PathVariable("id") String id);
}
