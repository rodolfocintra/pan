package br.com.pan.client.model;

public class MunicipioClientResponse {


	private Integer id;

	private String nome;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "MunicipioClientResponse [id=" + id + ", nome=" + nome + "]";
	}
	
	
}
