package br.com.pan.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.pan.cliente.api.model.Endereco;

@FeignClient(name = "${client.feing.cep.name}",url="${client.feing.cep.url}")
public interface CepClient {
	
	@RequestMapping(method = RequestMethod.GET, value = "{cep}/json")
    Endereco buscaPorCep(@PathVariable("cep") String cep);

}
