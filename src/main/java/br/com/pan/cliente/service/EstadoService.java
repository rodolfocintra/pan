package br.com.pan.cliente.service;

import java.util.List;

import br.com.pan.cliente.api.model.Estado;

public interface EstadoService {

	
	List<Estado> getEstados();
}
