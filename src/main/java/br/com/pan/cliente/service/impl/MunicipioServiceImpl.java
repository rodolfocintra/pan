package br.com.pan.cliente.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.pan.client.IbgeClient;
import br.com.pan.cliente.api.model.Municipio;
import br.com.pan.cliente.service.MunicipioService;

@Component
public class MunicipioServiceImpl implements MunicipioService{

	@Inject
	private IbgeClient ibgeClient;
	
	@Override
	public List<Municipio> getMunicipio(String idEstado) {
		return ibgeClient.getMunicipio(idEstado);
	}

}
