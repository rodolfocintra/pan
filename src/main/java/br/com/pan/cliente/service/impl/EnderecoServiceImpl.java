package br.com.pan.cliente.service.impl;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.pan.client.CepClient;
import br.com.pan.cliente.api.model.Endereco;
import br.com.pan.cliente.service.EnderecoService;
import br.com.pan.model.EnderecoEntity;
import br.com.pan.model.builder.EnderecoEntityBuilder;
import br.com.pan.repository.EnderecoRepository;

@Component
public class EnderecoServiceImpl implements EnderecoService {

	private CepClient cepClient;

	private EnderecoRepository enderecoRepository;

	@Inject
	public EnderecoServiceImpl(CepClient cepClient, EnderecoRepository enderecoRepository) {
		this.cepClient = cepClient;
		this.enderecoRepository = enderecoRepository;
	}

	@Override
	public Endereco getEndereco(String cep) {

		return cepClient.buscaPorCep(cep);
	}

	@Override
	public void save(Endereco endereco) {
		enderecoRepository.save(EnderecoEntityBuilder.builder()
				                                     .withBairro(endereco.getBairro())
				                                     .withCep(endereco.getCep())
				                                     .withComplemento(endereco.getComplemento())
				                                     .withLogradouro(endereco.getLogradouro())
				                                     .withNumero(endereco.getNumero())
				                                     .withUf(endereco.getUf()).build());
	 

	}

}
