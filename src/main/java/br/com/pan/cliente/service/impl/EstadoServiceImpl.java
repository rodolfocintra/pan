package br.com.pan.cliente.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.pan.client.IbgeClient;
import br.com.pan.cliente.api.model.Estado;
import br.com.pan.cliente.service.EstadoService;

@Component
public class EstadoServiceImpl implements EstadoService{

	@Inject
	private IbgeClient ibgeClient;
	
	@Override
	public List<Estado> getEstados() {
		return ordenaEstados(ibgeClient.getEstados());
	}

	private List<Estado> ordenaEstados(List<Estado> estados){
		
		List<Estado> estadosOrdenados = new ArrayList<Estado>();
		Estado sp = estados.stream().filter(e-> e.getSigla().equals("SP")).findFirst().get();
		estadosOrdenados.add(0,sp);
		estados.remove(sp);
		Estado rj = estados.stream().filter(e-> e.getSigla().equals("RJ")).findFirst().get();
		estadosOrdenados.add(1,rj);
		estados.remove(rj);
		estados.sort((e1,e2) -> e1.getNome().compareTo(e2.getNome()));
		estadosOrdenados.addAll(estados);
		return estadosOrdenados;
	}
}
