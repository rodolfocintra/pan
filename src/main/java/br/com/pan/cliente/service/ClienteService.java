package br.com.pan.cliente.service;

import br.com.pan.cliente.api.model.Cliente;

public interface ClienteService {

	
	void save(Cliente cliente);
	
	Cliente getCliente(Long id);
	
	
}
