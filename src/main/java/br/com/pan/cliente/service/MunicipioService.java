package br.com.pan.cliente.service;

import java.util.List;

import br.com.pan.cliente.api.model.Municipio;

public interface MunicipioService {
	
	List<Municipio> getMunicipio(String idEstado);

}
