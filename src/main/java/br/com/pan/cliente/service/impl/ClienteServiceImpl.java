package br.com.pan.cliente.service.impl;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.pan.cliente.api.model.Cliente;
import br.com.pan.cliente.api.model.Endereco;
import br.com.pan.cliente.api.model.builder.ClienteBuilder;
import br.com.pan.cliente.api.model.builder.EnderecoBuilder;
import br.com.pan.cliente.service.ClienteService;
import br.com.pan.model.ClienteEntity;
import br.com.pan.model.builder.ClienteEntityBuilder;
import br.com.pan.model.builder.EnderecoEntityBuilder;
import br.com.pan.repository.ClienteRepository;

@Component
public class ClienteServiceImpl implements ClienteService{

	@Inject
	private ClienteRepository clienteRepository;
	
	@Override
	public void save(Cliente cliente) {
		clienteRepository.save(ClienteEntityBuilder.builder()
				.withNome(cliente.getNomeCompleto())
				.withEndereco(EnderecoEntityBuilder.builder()
						                           .withBairro(cliente.getEndereco().getBairro())
						                           .withCep(cliente.getEndereco().getCep())
						                           .withComplemento(cliente.getEndereco().getComplemento())
						                           .withLogradouro(cliente.getEndereco().getLogradouro())
						                           .withNumero(cliente.getEndereco().getNumero())
						                           .withUf(cliente.getEndereco().getUf()).build()).build());		
	}

	@Override
	public Cliente getCliente(Long id) {
		ClienteEntity clientEntity = clienteRepository.findOneById(id);
		
		return ClienteBuilder.builder().withId(clientEntity.getId())
				                       .withNome(clientEntity.getNome())
				                       .withEndereco(EnderecoBuilder.builder()
				                    		                        .withBairro(clientEntity.getEndereco().getBairro())
				                    		                        .withCep(clientEntity.getEndereco().getCep())
				                    		                        .withComplemento(clientEntity.getEndereco().getComplemento())
				                    		                        .withId(clientEntity.getEndereco().getId())
				                    		                        .withLogradouro(clientEntity.getEndereco().getLogradouro())
				                    		                        .withNumero(clientEntity.getEndereco().getNumero())
				                    		                        .withUf(clientEntity.getEndereco().getUf()).build()).build();
	}

	
	
	
	
}
