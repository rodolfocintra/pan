package br.com.pan.cliente.service;

import br.com.pan.cliente.api.model.Endereco;

public interface EnderecoService {

	Endereco getEndereco(String cep);
	
	void save(Endereco endereco);
	
}
