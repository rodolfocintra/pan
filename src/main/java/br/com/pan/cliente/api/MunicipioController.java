package br.com.pan.cliente.api;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.com.pan.cliente.api.model.Municipio;
import br.com.pan.cliente.service.MunicipioService;

@RestController
public class MunicipioController implements MunicipioApi{

	
	@Inject
	private MunicipioService municipioService;
	
	@Override
	public ResponseEntity<List<Municipio>> municipioIdGet(@PathVariable("id") String id) {
	
		return new ResponseEntity<List<Municipio>>(municipioService.getMunicipio(id),HttpStatus.OK);
	}

}
