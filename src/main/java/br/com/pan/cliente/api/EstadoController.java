package br.com.pan.cliente.api;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import br.com.pan.cliente.api.model.Estado;
import br.com.pan.cliente.service.EstadoService;

@RestController
public class EstadoController implements EstadoApi{

	@Inject
	private EstadoService estadoService;
	@Override
	public ResponseEntity<List<Estado>> estadoGet() {
		
		return new ResponseEntity<List<Estado>>(estadoService.getEstados(),HttpStatus.OK);
	}

}
