package br.com.pan.cliente.api.model.builder;

import br.com.pan.cliente.api.model.Endereco;
import br.com.pan.model.EnderecoEntity;
import br.com.pan.model.builder.Builder;
import br.com.pan.model.builder.EnderecoEntityBuilder;

public class EnderecoBuilder implements Builder<Endereco>{

private Endereco endereco;
	
	
	
	public EnderecoBuilder() {
		this.endereco = new Endereco();
	}

	public static EnderecoBuilder builder() {
		return new EnderecoBuilder();
	}
	
	public EnderecoBuilder withId(Long id) {
		this.endereco.setId(id.toString());
		return this;
	}

	public EnderecoBuilder withCep(String cep) {
		this.endereco.setCep(cep);
		return this;
	}
	
	public EnderecoBuilder withLogradouro(String logradouro) {
		this.endereco.setLogradouro(logradouro);
		return this;
	}
	public EnderecoBuilder withNumero(String numero) {
		this.endereco.setNumero(numero);
		return this;
	}
	
	public EnderecoBuilder withComplemento(String complemento) {
		this.endereco.setComplemento(complemento);
		return this;
	}
	
	public EnderecoBuilder withBairro(String bairro) {
		this.endereco.setBairro(bairro);
		return this;
	}
	
	public EnderecoBuilder withUf(String uf) {
		this.endereco.setUf(uf);
		return this;
	}
	

	@Override
	public Endereco build() {
		return this.endereco;
	}

}
