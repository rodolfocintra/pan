package br.com.pan.cliente.api;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import br.com.pan.cliente.api.model.Endereco;
import br.com.pan.cliente.service.EnderecoService;

@RestController
public class EnderecoController implements EnderecoApi{

	@Inject
	private EnderecoService enderecoService;
	
	@Override
	public ResponseEntity<Endereco> enderecoCepGet(String cep) {
		enderecoService.getEndereco(cep);
		return new ResponseEntity<>(enderecoService.getEndereco(cep),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Void> updateEndereco(@Valid Endereco body) {
		enderecoService.save(body);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

		
}
