package br.com.pan.cliente.api.model.builder;

import br.com.pan.cliente.api.model.Cliente;
import br.com.pan.cliente.api.model.Endereco;
import br.com.pan.model.ClienteEntity;
import br.com.pan.model.EnderecoEntity;
import br.com.pan.model.builder.Builder;
import br.com.pan.model.builder.ClienteEntityBuilder;

public class ClienteBuilder implements Builder<Cliente>{

	private Cliente cliente;
	
	public ClienteBuilder() {
		this.cliente = new Cliente();
	}

	public static ClienteBuilder builder() {
		return new ClienteBuilder();
	}
	
	public ClienteBuilder withId(Long id) {
		this.cliente.setId(id);
		return this;
	}

	public ClienteBuilder withNome(String nome) {
		this.cliente.setNomeCompleto(nome);
		return this;
	}
	
	public ClienteBuilder withEndereco(Endereco endereco) {
		this.cliente.setEndereco(endereco);
		return this;
	}
		
	@Override
	public Cliente build() {
		return this.cliente;
	}


}
