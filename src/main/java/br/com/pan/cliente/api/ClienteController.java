package br.com.pan.cliente.api;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.pan.cliente.api.model.Cliente;
import br.com.pan.cliente.service.ClienteService;

@RestController
public class ClienteController implements ClienteApi{

	@Inject
	private ClienteService clienteService;
	
	@Override
	public ResponseEntity<Void> addClient(@Valid @RequestBody  Cliente body) {
		clienteService.save(body);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Cliente> getClienteById( @PathVariable("id") Long id) {
		return new ResponseEntity<Cliente>(clienteService.getCliente(id),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Void> updateCliente(@Valid @RequestBody Cliente body) {
		clienteService.save(body);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
